﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(WSCredential))]
public class EditorCredential : Editor {

    WSCredential obj;

    void Awake()
    {
        if (obj == null)
            obj = (WSCredential)target;
    }

    public override void OnInspectorGUI()
    {
        obj.Username = EditorGUILayout.TextField("Username :", obj.Username);
        obj.Password = EditorGUILayout.TextField("Password :", obj.Password);
        if (GUILayout.Button("Save"))
        {
            EditorUtility.SetDirty(obj);
        }
    }

}
