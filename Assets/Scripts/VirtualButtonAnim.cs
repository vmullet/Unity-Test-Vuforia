﻿using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class VirtualButtonAnim : MonoBehaviour,IVirtualButtonEventHandler {

    public static string ROTATE_ANIM = "Rotate";
    public static string OPEN_ANIM = "Open";
    public static string SCREENSHOT_FILENAME = "Screenshot.png";
    public GameObject virtualButton;
    public Animator cubeAnimator;
    public Animator panelAnimator;
    public TextMesh textButton;
    public Text alprStatus;
    private int clickCount;


    // Use this for initialization
    void Start () {
        virtualButton.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
        clickCount = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        increaseClick();
        cubeAnimator.SetBool(ROTATE_ANIM, true);
        panelAnimator.SetBool(OPEN_ANIM, true);
        ScreenCapture.CaptureScreenshot(SCREENSHOT_FILENAME);
        alprStatus.text = "Photo prise " + Application.persistentDataPath + "/" + SCREENSHOT_FILENAME;
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        cubeAnimator.SetBool(ROTATE_ANIM, false);
        panelAnimator.SetBool(OPEN_ANIM, false);
        alprStatus.text = "En attente";
    }

    private void increaseClick()
    {
        clickCount++;
        textButton.text = clickCount.ToString();
    }

}
