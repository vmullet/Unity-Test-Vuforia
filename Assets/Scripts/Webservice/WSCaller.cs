﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;

public class WSCaller : MonoBehaviour {

    public static string URL = "https://ticketsdev.eurotunnel5.com/myeurotunnelws/myetbookrecall.asmx";
    public static string SOAP_ENVELOPE = "<?xml version='1.0' encoding='utf-8'?>\n"
            + "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>\n"
           + "<soap:Body>\n"
           + "<myetbookrecall xmlns='https://tickets.eurotunnel.com/webservices/'>\n"
                 + "<input>{0}</input>\n"
                  + "</myetbookrecall>\n"
                + "</soap:Body>\n"
               + "</soap:Envelope>";
    public static string INNER_SOAP_REQUEST = "&lt;?xml version=&quot;1.0&quot; encoding=&quot;iso-8859-1&quot;?&gt;\n"
    + "&lt;myet-bookrecallreq xmlns:xsi=&quot;http://www.w3.org/2001/xmlschema-instance&quot; \n"
    + "xmlns:xsd=&quot;http://www.w3.org/2001/xmlschema&quot;&gt; \n"
    + "&lt;requestor horodate=&quot;{0}&quot; source=&quot;ppci&quot; username=&quot;art&quot; api-user=&quot;{1}&quot;\n"
    + "api-pwd=&quot;{2}&quot; /&gt;\n"
    + "&lt;leg veh-registration=&quot;{3}&quot; /&gt;\n"
    + "&lt;/myet-bookrecallreq&gt;";

    public static string CONTENT_TYPE = "text/xml";
    public static string SOAP_ACTION = "https://tickets.eurotunnel.com/webservices/myetbookrecall";
    public static string XPATH_BOOKING = "myet-bookrecallans/requestor/booking";

    public WSCredential credential;
    public Text myText; // Just for debug

    void Start()
    {
        StartCoroutine(CallService("aa-123-bb"));
        StartCoroutine(CallService("be011yy"));
    }

    private IEnumerator CallService(string vehicleRegistration)
    {
        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", CONTENT_TYPE);
        headers.Add("SOAPAction", SOAP_ACTION);
        WWW www = new WWW(URL, System.Text.Encoding.UTF8.GetBytes(BuildSoapRequest(vehicleRegistration)), headers);
        yield return www;
        XmlDocument response = ParseXmlResponse(www.text);        
        BookingData bookingData = new BookingData(response.SelectSingleNode(XPATH_BOOKING));
        myText.text += bookingData.ToString() + "\n\n"; // Just for debug
        yield return null;
    }

    private string BuildSoapRequest(string vehicleRegistration)
    {
        string date = DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss");
        return string.Format(SOAP_ENVELOPE, 
            string.Format(INNER_SOAP_REQUEST,date, credential.Username,credential.Password,vehicleRegistration));
    }

    private XmlDocument ParseXmlResponse(string content)
    {
        XmlDocument doc = new XmlDocument();
        doc.LoadXml(content);
        XmlDocument subDoc = new XmlDocument();
        subDoc.LoadXml(doc.InnerText);
        return subDoc;
    }

}
