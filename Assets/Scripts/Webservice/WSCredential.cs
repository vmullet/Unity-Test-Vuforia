﻿using UnityEngine;

[CreateAssetMenu(fileName = "WSCredential", menuName = "Webservice/Credential", order = 1)]
[PreferBinarySerialization]
public class WSCredential : ScriptableObject {

    [SerializeField]
    private string username;
    [SerializeField]
    private string password;

    public string Username
    {
        get { return username; }
        set { username = value; }
    }

    public string Password
    {
        get { return password; }
        set { password = value; }
    }

    public WSCredential(string username,string password)
    {
        this.username = username;
        this.password = password;
    }
	
}
