﻿using System;
using System.Xml;

public class BookingData {

    private string clientFirstname;
    private string clientSurname;
    private DateTime outboundDate;
    private string vehicleRegistration;
    private Boolean isPremium = false;
    private string dateStatus;
    private string parkingZone;

    public BookingData(XmlNode dataNode)
    {
        LoadData(dataNode);
    }

    public void LoadData(XmlNode dataNode)
    {
        var nameData = dataNode.SelectSingleNode("name").Attributes;
        var outboundData = dataNode.SelectSingleNode("outbound-journey").Attributes;
        var vehicleData = dataNode.SelectSingleNode("vehicle").Attributes;
        clientFirstname = nameData["firstname"].Value;
        clientSurname = nameData["surname"].Value;
        outboundDate = DateTime.Parse(outboundData["datetime"].Value);
        CheckParkingAndStatus();
        isPremium = outboundData["faretype"].Value != "standard";
        vehicleRegistration = vehicleData["registration"].Value;
    }

    private void CheckParkingAndStatus()
    {
        double diff = outboundDate.Subtract(DateTime.Now).TotalMinutes;
        if (diff >= 60)
        {
            dateStatus = "Early";
            parkingZone = "Zone 2";
        }
        else if (diff >= 0)
        {
            dateStatus = "On Time";
            parkingZone = "Zone 1";
        }
        else
        {
            dateStatus = "Late";
            parkingZone = "Zone 2";
        }
    }

    public override string ToString()
    {
        string data = "";
        data += "Nom : " + clientSurname + "\n";
        data += "Prénom : " + clientFirstname + "\n";
        data += "Date : " + outboundDate.ToString("dd/MM/yyyy HH:mm") + "\n";
        data += "Immatriculation : " + vehicleRegistration + "\n";
        data += "Premium : " + (isPremium ? "Oui" : "Non") + "\n";
        data += "Statut Horaire : " + dateStatus + "\n";
        data += "Parking : " + parkingZone + "\n";
        return data;
    }


}
